#!/usr/bin/env python3
import json
import  requests

response = requests.post(
    "https://nomad-lab.eu/prod/v1/api/v1/entries/query",
    json={
        "owner": "visible",
        "query": {},
        "aggregations": {
            "results.method.simulation.dft.xc_functional_names": {
            "terms": {
                "quantity": "results.method.simulation.dft.xc_functional_names",
                "size": 2000
                     }
            },
        },
        "pagination": {
            "page_size": 0
        }
    }
)

value_branch     = response.json()['aggregations']['results.method.simulation.dft.xc_functional_names']['terms']['data']
functional_names = {func['value']: func['count'] for func in value_branch}

with open('functionals_registered.json', 'w') as f:
    json.dump(functional_names, f)
