#!/usr/bin/env python3
import json
import re

# Read functional list from libxc
libxc_header_file = open('../xc_funcs.h', 'r')
libxc_headers     = libxc_header_file.readlines()
libxc_header_file.close()

# Generate dictionary (name: index)
functionals_libxc = {}
for line in libxc_headers:
    line = line.strip().split()
    functional_name = re.sub(r'^XC_', '', line[1])
    functionals_libxc[functional_name] = int(line[2])

# Read functionals registered in NOMAD
# TODO make dynamic in the future
functionals_registered_file = open('functionals_registered.json', 'r')
functionals_registered      = json.load(functionals_registered_file)
functionals_registered_file.close()

# Apply libxc indexing to NOMAD functionals
functionals_selected = {}
for functional_name in functionals_registered.keys():
    try:
        functionals_selected[functional_name] = functionals_libxc[functional_name]
    except:
        pass
functionals_selected = dict(sorted(functionals_selected.items(), key=lambda x:x[1])) # sort in line with LIBXC order

# Write out new dictionary (NOMAD name: libxc index)
functionals_selected_file = open('functionals_selected.json', 'w')
json.dump(functionals_selected, functionals_selected_file)
functionals_selected_file.close()
