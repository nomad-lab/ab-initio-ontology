# AreaC-Theory_and_Computation-AppDef
Repository for Theory and Computation Ontologies in line with Work Packages C1.1, C1.2, and C3.1
The current scope are DFT and other ab initio methods.

## Guidelines
We present a knowledge base and its terminology/logic (ontology) in function of Density Functionals at large and based on the NOMAD Metadata at hand. As such, we aim to make our ontology
- __Intuitive__: use terminology that easy to understand or is to be expected by the user. Always present non-abbreviated forms (with abbreviations being optional)
- __Concrete__: only introduce terms which will be used for containing entries and yield non-empty queries
- __Hierarchical__: guide users towards physically superior models follwing this logic scheme [^1]. Rather, employ multiple _semantic_ perspectives
- __General__: repurpose logic before inventing a new one. Similarly, extend the logic from one section out as much as possible
- __Traceable__: add as much citations and documentation as possible using rdfs:isDefinedBy

### Density Functionals
- __Sources__:
    - libxc
    - libvdwxc
    - …
- __Semantic hierarchies__:
  - Composition
  - Jacob's Ladder
  - Fitting sets
      - Molecules covered
      - Properties covered
  - Comparison with the universal Density Functional's behaviour (in known limiting cases)?

### Special Relativistics

## Software
The ontology is generated in _hatch_fowl_ using Owlready2.
The end result (including reasoning) is always stored in OWL/ntuples format, which can be read and further edited by [Protege](https://protege.stanford.edu/).
Several versions are constructed with leaner version based on use cases.

The API is supported in fowl_nomad.py and supports classifying known individuals or new constructions.

# Footnotes
[^1]: The purpose of this approach is to construct a more complete model, even if the extent of the physics included is hard to gauge. When looking at a data set or query, the user can now generate a categorical axis with a well-defined direction, which can be a first step to _clustering_ or _trend analysis_ in Machine Learning.