import owlready2 as owl


def main(onto):
    """"""
    with onto:
        class nomad_jacobsladder(owl.label): pass
        class RungOnJacobsLadder(owl.Thing): pass

        class LocalDensityApproximation(RungOnJacobsLadder): pass
        LocalDensityApproximation.abbrev.append('LDA')
        LocalDensityApproximation.nomad_jacobsladder.append(1)
        LocalDensityApproximation.equivalent_to = [
            onto.DensityFunctionalTheory &
            onto.has_input.value(onto.ElectronDensity) &
            onto.has_input.exactly(1, onto.DensityType)
        ]
        class GeneralizedGradientApproximation(RungOnJacobsLadder): pass
        GeneralizedGradientApproximation.abbrev.append('GGA')
        GeneralizedGradientApproximation.nomad_jacobsladder.append(2)
        GeneralizedGradientApproximation.equivalent_to = [
            onto.DensityFunctionalTheory &
            onto.has_input.value(onto.ElectronDensity) &
            onto.has_input.value(onto.ElectronDensityGradient) &
            onto.has_input.exactly(2, onto.DensityType)
        ]
        class metaGeneralizedGradientApproximation(RungOnJacobsLadder): pass
        metaGeneralizedGradientApproximation.abbrev.append('Meta-GGA')
        metaGeneralizedGradientApproximation.nomad_jacobsladder.append(3)
        metaGeneralizedGradientApproximation.equivalent_to = [
            onto.DensityFunctionalTheory &
            onto.has_input.value(onto.ElectronDensity) &
            onto.has_input.value(onto.ElectronDensityGradient) &
            (onto.has_input.value(onto.ElectronDensityLaplacian) |
             onto.has_input.value(onto.KineticEnergyDensity)) &
            onto.has_input.exactly(3, onto.DensityType)
        ]
        owl.AllDisjoint([LocalDensityApproximation, GeneralizedGradientApproximation, metaGeneralizedGradientApproximation])

        class HybridFunctional(RungOnJacobsLadder): pass
        HybridFunctional.equivalent_to = [
            onto.DensityFunctionalTheory &
            onto.has_component.some(onto.ExactExchange)
        ]
        class has_weight_mixing_parameter(HybridFunctional >> float): pass

        class RandomPhaseApproximation(RungOnJacobsLadder): pass
        RandomPhaseApproximation.abbrev.append('RPA')
        RandomPhaseApproximation.nomad_jacobsladder.append(6)

        class ShortRangeHybridFunctional(HybridFunctional): pass
        class has_weight_screening_parameter(ShortRangeHybridFunctional >> float): pass

        class HybridLocalDensityApproximation(RungOnJacobsLadder): pass
        HybridLocalDensityApproximation.equivalent_to = [
            HybridFunctional & LocalDensityApproximation
        ]
        class HybridGeneralizedGradientApproximation(RungOnJacobsLadder): pass
        HybridGeneralizedGradientApproximation.abbrev.append('Hybrid')
        HybridGeneralizedGradientApproximation.nomad_jacobsladder.append(4)
        HybridGeneralizedGradientApproximation.equivalent_to = [
            HybridFunctional & GeneralizedGradientApproximation
        ]
        class hyperGeneralizedGradientApproximation(RungOnJacobsLadder): pass
        hyperGeneralizedGradientApproximation.abbrev.append('Hyper-GGA')
        hyperGeneralizedGradientApproximation.nomad_jacobsladder.append(5)
        hyperGeneralizedGradientApproximation.equivalent_to = [
            HybridFunctional & metaGeneralizedGradientApproximation
        ]
        hyperGeneralizedGradientApproximation.isDefinedBy.append(
            """@inproceedings{author = {Perdew, John P. and Schmidt, Karla}, booktitle = {AIP Conference Proceedings}, doi = {10.1063/1.1390175},
            issn = {0094243X}, month = {aug}, pages = {1--20}, publisher = {AIP},
            title = {{Jacob's ladder of density functional approximations for the exchange-correlation energy}},
            url = {http://aip.scitation.org/doi/abs/10.1063/1.1390175}, volume = {577}, year = {2001}}"""
        )
        # onto.Hubbard.isDefinedBy.append("""author = {Anisimov, Vladimir I. and Gunnarsson, O.}, doi = {10.1103/PhysRevB.43.7570},
        #                              issn = {0163-1829}, journal = {Physical Review B},
        #                              month = {apr}, number = {10}, pages = {7570--7574},
        #                              title = {{Density-functional calculation of effective Coulomb interactions in metals}},
        #                              url = {https://link.aps.org/doi/10.1103/PhysRevB.43.7570}, volume = {43}, year = {1991}}""")
        # onto.Hubbard.isDefinedBy.append("""author = {Anisimov, Vladimir I. and Aryasetiawan, F. and Lichtenstein, A. I.}, doi = {10.1088/0953-8984/9/4/002},
        #                              issn = {09538984}, journal = {Journal of Physics Condensed Matter}, number = {4}, pages = {767--808},
        #                              title = {{First-principles calculations of the electronic structure and spectra of strongly correlated systems: The LDA + U method}},
        #                              volume = {9}, year = {1997}}""")

        class DoubleHybridApproximation(RungOnJacobsLadder): pass
        class extends_with_ResponseFunctional(onto.extends):
            owl.rdf_domain = [DoubleHybridApproximation]
            owl.rdf_range = [HybridFunctional]

        class DoubleHybridLocalDensityApproximation(RungOnJacobsLadder): pass
        DoubleHybridLocalDensityApproximation.equivalent_to = [
            DoubleHybridApproximation & LocalDensityApproximation
        ]
        class DoubleHybridGeneralizedGradientApproximation(RungOnJacobsLadder): pass
        DoubleHybridGeneralizedGradientApproximation.abbrev.append('Double Hybrid')
        DoubleHybridGeneralizedGradientApproximation.nomad_jacobsladder.append(6)
        DoubleHybridGeneralizedGradientApproximation.equivalent_to = [
            DoubleHybridApproximation & GeneralizedGradientApproximation
        ]
        class DoubleHyperGeneralizedGradientApproximation(RungOnJacobsLadder): pass
    return onto
