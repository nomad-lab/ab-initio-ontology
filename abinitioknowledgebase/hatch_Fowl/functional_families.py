#!/usr/bin/env python3
import owlready2 as owl

class LibxcMap():
    """"""
    libxc_map = {}
    
    def _x_c_name(self, x_c_entry):
        """"""
        for classification in x_c_entry.is_a:
            if classification.name in ['Correlation', 'Exchange']:
                return classification.name

    def write_to_map(self, xc_entry):
        """"""
        x_c_entries = []
        for component in xc_entry.has_component:
            if onto.Correlation in component.is_a: # onto.Exchange
                x_c_entries += component

        if len(x_c_entries) == 2:
            map_key = x_c_entries.sort(key=self._x_c_name)
            libxc_map[tuple(map_key)] = xc_entry.name


def main(onto):
    """"""
    with onto:
        # LDAs
        onto.LocalDensityApproximation('SlaterExchange')
        onto.SlaterExchange.equivalent_to.append(onto.X)
        onto.ExchangeCorrelation('PW92')
        onto.PW92.is_a.append(onto.LocalDensityApproximation)
        for component in [onto.SlaterExchange, onto.C_PW]:
            onto.PW92.has_component.append(component)
        onto.ExchangeCorrelation('PerdewWang92')
        onto.PerdewWang92.equivalent_to.append(onto.PW92)

        # PBE FAMILY
        onto.ExchangeCorrelation('PerdewBurkeErnzerhof')
        onto.PerdewBurkeErnzerhof.equivalent_to.append(onto.PBE)

        onto.ExchangeCorrelation('PBE_R')
        # for component in [onto.C_PBE, onto.X_PBE_R]:
            # onto.PBE_R.has_component.append(component)
        onto.ExchangeCorrelation('revPBE')
        onto.ExchangeCorrelation('revisedPerdewBurkeErnzerhof')
        onto.revPBE.equivalent_to.append(onto.PBE_R)
        onto.revisedPerdewBurkeErnzerhof.equivalent_to.append(onto.PBE_R)

        # PBE hybrid FAMILY
        for func in [onto.HSE06, onto.HSE03, onto.PBEH]:
            func.has_weight_mixing_parameter.append(.25)
            # for component in [onto.X_PBE, onto.C_PBE]:
                # func.has_component.append(component)

        # onto.HSE06.has_weight_screening_parameter.append(.2)
        # onto.HSE03.has_weight_screening_parameter.append(.3)
        onto.GeneralizedGradientApproximation('PBE0')
        onto.PBE0.equivalent_to.append(onto.PBEH)

        # B3 FAMILY
        class B3family(onto.ExchangeCorrelation): pass
        B3family.is_a.append(onto.HybridFunctional)
        B3family.isDefinedBy.append('Stephen1994')
        B3family.equivalent_to = [
            onto.has_component.value(onto.X_B88) &
            onto.has_component.min(2, onto.Exchange)
        ]

        # for component in [onto.SlaterExchange, onto.X_B88, onto.C_PW, onto.C_PW91]:
            # onto.B3PW91.has_component.append(component)
        onto.B3PW91.has_weight_mixing_parameter.append(.2)

        class B3LYPfamily(onto.B3family): pass
        B3LYPfamily.isDefinedBy.append('Stephen1994')
        B3LYPfamily.equivalent_to = [
            B3family &
            onto.has_component.value(onto.C_LYP) &
            onto.has_component.min(2, onto.Correlation)
        ]

        for component in [onto.SlaterExchange, onto.X_B88, onto.C_LYP]:
            for blyp in [onto.B3LYP, onto.B3LYP3, onto.B3LYP5]:
                blyp.has_component.append(component)
                blyp.has_weight_mixing_parameter.append(.2)
        # onto.B3LYP.has_component.append(onto.C_VWN_RPA)
        # onto.B3LYP3.has_component.append(onto.C_VWN_3)
        # onto.B3LYP5.has_component.append(onto.C_VWN)

    return onto
