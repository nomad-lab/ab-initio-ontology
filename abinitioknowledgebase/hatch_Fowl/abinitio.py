import owlready2 as owl


def main(onto):
    """Define AbInitio branch"""
    with onto:
        class starts_from(owl.Thing >> owl.Thing): pass

        # classes and individuals
        class AbInitio(owl.Thing): pass

        class DensityFunctionalTheory(AbInitio): pass
        onto.DensityFunctionalTheory.abbrev.append('DFT')
        onto.DensityFunctionalTheory.equivalent_to = [
            onto.has_component.some(onto.Correlation) &
            onto.has_component.some(onto.Exchange) &
            onto.has_input.some(onto.DensityType)
        ]
        class DynamicMeanFieldTheory(AbInitio): pass
        class GreensFunctionApproach(AbInitio): pass
        class ReducedDensityMatrixFunctionalTheory(AbInitio): pass
        class WaveFunctionApproach(AbInitio): pass
        WaveFunctionApproach.equivalent_to = [
            onto.has_input.some(onto.WavefunctionType)
        ]
        WaveFunctionApproach('HartreeFock')
        onto.HartreeFock.abbrev.append('HF')
        onto.HartreeFock.has_component.append(onto.ExactExchange)

        class LibxcFunctional(DensityFunctionalTheory): pass
        class EnsembleDensityFunctionalTheory(DensityFunctionalTheory): pass
        class KohnShamApproximation(DensityFunctionalTheory): pass
        onto.KohnShamApproximation.label.append('Assumption: slowly varying electron density')
        onto.KohnShamApproximation.abbrev.append('KS-DFT')
        class OrbitalFreeTheory(DensityFunctionalTheory): pass
        class ContinuousTimeQuantumMonteCarlo(DynamicMeanFieldTheory): pass
        class HamiltonianBath(DynamicMeanFieldTheory): pass
        class BetheSalpeterEquation(GreensFunctionApproach): pass
        class GW(GreensFunctionApproach): pass
        onto.GW('G0W0')
        onto.GW('scGW')
        onto.GW('scGW0')
        onto.GW('scG0W')
        onto.GW('ev-scGW')
        onto.GW('qp-scGW')
        class postHartreeFock(WaveFunctionApproach): pass
        postHartreeFock.equivalent_to = [
            starts_from.value(onto.HartreeFock)
        ]
        class QuantumMonteCarlo(WaveFunctionApproach): pass

        class DensityFunctionalTheoryPlusU(DensityFunctionalTheory): pass
        DensityFunctionalTheoryPlusU.abbrev.append('DFT+U')
        class has_hubbard_correction(DensityFunctionalTheoryPlusU >> float): pass
        class has_hubbard_u(has_hubbard_correction):
            owl.rdf_domain = [DensityFunctionalTheoryPlusU]
            owl.rdf_range = [float]
        class has_hubbard_j(has_hubbard_correction):
            owl.rdf_domain = [DensityFunctionalTheoryPlusU]
            owl.rdf_range = [float]
        class has_hubbard_u_effective(has_hubbard_correction):
            owl.rdf_domain = [DensityFunctionalTheoryPlusU]
            owl.rdf_range = [float]
        class OptimizedEffectivePotential(DensityFunctionalTheory): pass
        class ConfigurationInteraction(postHartreeFock): pass
        class CoupledCluster(postHartreeFock): pass
        class MollerPlesset(postHartreeFock): pass
    return onto
