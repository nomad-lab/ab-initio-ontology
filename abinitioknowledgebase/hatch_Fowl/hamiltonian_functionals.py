import owlready2 as owl


def main(onto):
    """"""
    with onto:
        # properties
        class has_input(owl.Thing >> onto.ElectronicRepresenation): pass
        class has_component(owl.Thing >> owl.Thing): pass
        owl.Imp().set_as_rule('has_component(?x, ?y), has_component(?y, ?z) -> has_component(?x, ?z)')
        owl.Imp().set_as_rule('has_component(?x, ?y), has_input(?y, ?z) -> has_input(?x, ?z)')
        class assumes(owl.ObjectProperty): pass
        class based_on(owl.ObjectProperty): pass

        # classes + indivduals
        class Approximation(owl.Thing): pass
        Approximation.equivalent_to = [
            assumes.some(owl.Thing) &
            based_on.some(owl.Thing)
        ]
        class Hamiltonian(owl.Thing): pass
        class SingleParticleHamiltonian(owl.Thing): pass
        class is_benchmarked_on(SingleParticleHamiltonian >> onto.DataSet): pass
        class TimeRelation(owl.Thing): pass

        class RelativisticHamiltonian(Hamiltonian): pass
        class NonRelativisticHamiltonian(Hamiltonian): pass
        class TimeDependent(TimeRelation): pass
        class TimeIndependent(TimeRelation): pass

        class ScalarRealtivisticHamiltonian(RelativisticHamiltonian): pass
        ScalarRealtivisticHamiltonian.equivalent_to = [
            assumes.value(onto.weak_potential_limit) &
            based_on.value(onto.exactFoldyWouthuysen)
        ]
        class Correlation(SingleParticleHamiltonian): pass
        for correlation in ('PT2', 'dRPA'):
            Correlation(correlation)
        class Exchange(SingleParticleHamiltonian): pass
        Exchange('ExactExchange')
        Exchange('X_HF')
        onto.X_HF.isDefinedBy.append('NOMAD')
        onto.ExactExchange.equivalent_to = [onto.X_HF]
        class ExternalPotential(SingleParticleHamiltonian): pass
        class KineticEnergy(SingleParticleHamiltonian): pass
        class VanDerWaals(SingleParticleHamiltonian): pass

        class ExchangeCorrelation(SingleParticleHamiltonian): pass
        ExchangeCorrelation.equivalent_to = [
            Exchange & Correlation
        ]
        class VanDerWaalsCorrelation(SingleParticleHamiltonian): pass
        VanDerWaalsCorrelation.abbrev.append('vdW')
        for vdw in ('GrimmeD1', 'GrimmeD2', 'GrimmeD3', 'GrimmeD4', 'Dion', 'Vydrov'):
            VanDerWaalsCorrelation(vdw)

        class ScreeningType(owl.Thing): pass
        for screening in ('ErrorFunction', 'YukawaKernel', 'GaussianAttenuation'):
            ScreeningType(screening)
        class ScreenedExchange(Exchange): pass
        ScreenedExchange.equivalent_to = [
            onto.has_component.value(onto.ExactExchange) &
            onto.has_component.some(ScreeningType)
        ]
        class DouglasKrollTransformation(ScalarRealtivisticHamiltonian): pass
        class RegularApproximation(ScalarRealtivisticHamiltonian): pass

        class ZerothOrderRegularApproximationFamily(RegularApproximation): pass
        ZerothOrderRegularApproximationFamily('ZerothOrderRegularApproximation')
        onto.ZerothOrderRegularApproximation.abbrev = ['ZORA']
        ZerothOrderRegularApproximationFamily('atomicZerothOrderRegularApproximation')
        onto.atomicZerothOrderRegularApproximation.abbrev = ['atomicZORA']
        ZerothOrderRegularApproximationFamily('scalarZerothOrderRegularApproximation')
        onto.scalarZerothOrderRegularApproximation.abbrev = ['scalarZORA']
    return onto
