#!/usr/bin/env python3
import os
import owlready2 as owl
import abinitio, functional_families, general, hamiltonian_functionals, jacobs_ladder, libxc_functionals


def main(iri):
    onto = owl.get_ontology(iri)
    with onto:
        general.main(onto)
        hamiltonian_functionals.main(onto)
        abinitio.main(onto)
        jacobs_ladder.main(onto)
        libxc_functionals.main(onto)
        functional_families.main(onto)
        # deduce additional information
        owl.sync_reasoner_pellet(infer_property_values=True, infer_data_property_values=True, debug=2)
    # save ontology
    onto.save(iri, format='ntriples')


local_folder = os.getcwd() + '/'
file_name = 'F'
version = '0.0.3'
iri = local_folder + file_name + '.owl'
main(iri)
