#!/usr/bin/env python3
import json
import owlready2 as owl
import pylibxc as xc
import re

def get_rawname(func_name) -> str:
    """Extract functional name from libxc
    Functionals here are considered XC
    if it is only X or C, it maintains the prefix"""
    delimiter = '_'
    func_name = re.sub('^HYB' + delimiter, '', func_name)
    for rung_tag in ('LDA', 'GGA', 'MGGA'):
        func_name = re.sub('^' + rung_tag + delimiter, '', func_name)
    func_name = re.sub('^XC' + delimiter, '', func_name)
    return func_name


def compare_xc(funcs):
    """"""
    xc_funcs = []
    if len(funcs) == 2:
        for func in funcs:
            for x_c in ('C', 'X'):
                xc_func = re.search(r'^' + x_c + '_(.*)', func.name)
                if xc_func is not None:
                    xc_funcs.append(xc_func.group(1))
                    break

    if len(xc_funcs) == 2:
        if xc_funcs[0] == xc_funcs[1]:
            return xc_funcs[0]


def main(onto):
    """"""
    with onto:
        func_kind_map = {0: onto.Exchange, 1: onto.Correlation, 2: onto.ExchangeCorrelation, 3: onto.KineticEnergy}
        func_jacob_map = {1: onto.LocalDensityApproximation, 2: onto.GeneralizedGradientApproximation, 4: onto.metaGeneralizedGradientApproximation}
        func_hyb_map = {2: onto.ErrorFunction, 3: onto.YukawaKernel, 4: onto.GaussianAttenuation, 5: onto.DoubleHybridApproximation}

        nomad_registered_functionals = json.load(open('queries/functionals_selected.json', 'r')).values()
        functionals_search_selection = list(nomad_registered_functionals) + [7, 8, 30, 105]
        for funcId in functionals_search_selection:
            func = xc.LibXCFunctional(funcId, spin='polarized')
            func_name = xc.util.xc_functional_get_name(funcId).upper()
            temp = owl.Thing(get_rawname(func_name))
            # Annotation
            temp.label.append('LibXC index: ' + str(funcId))
            for bib_entry in func._bibtexs:
                temp.isDefinedBy.append(bib_entry)
            # Assign FunctionalType
            try:
                temp.is_a.append(func_kind_map[func._kind])
            except:
                pass
            # Assign Jacob's Ladder
            try:
                temp.is_a.append(func_jacob_map[func._family])
            except:
                pass
            # Parse hybrids
            if func._hyb_type > 0:
                temp.is_a.append(onto.HybridFunctional)
            try:
                temp.has_component.append(func_hyb_map[func._hyb_type])
            except:
                pass
        # Add XC combinations when not present
        for c in onto.get_instances_of(onto.Correlation):
            for x in onto.get_instances_of(onto.Exchange):
                comparison_xc = compare_xc([c, x])
                if comparison_xc is not None:
                    temp = onto.ExchangeCorrelation(comparison_xc)
                    temp.has_component = [c, x]
                    component_classes = c.is_a + x.is_a
                    if onto.metaGeneralizedGradientApproximation in component_classes:
                        temp.is_a.append(onto.metaGeneralizedGradientApproximation)
                    elif onto.GeneralizedGradientApproximation in component_classes:
                        temp.is_a.append(onto.GeneralizedGradientApproximation)
                    elif onto.LocalDensityApproximation in component_classes:
                        temp.is_a.append(onto.LocalDensityApproximation)
        # Manually connect functionals
        #onto.B2PLYP.has_component.append(onto.PT2)
    return onto
