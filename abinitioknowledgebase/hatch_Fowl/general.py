import owlready2 as owl


def main(onto):
    """"""
    with onto:
        # labels
        class abbrev(owl.label): pass
        class Abbreviation(owl.Thing): pass

        # classes and individuals
        class DataSet(owl.Thing): pass
        class has_reference_properties(DataSet >> str): pass
        class has_reference_methods(DataSet >> str): pass
        class extends(owl.ObjectProperty): pass
        class extends_DataSet(extends):
            owl.rdf_domain = [DataSet]
            owl.rdf_range = [DataSet]
        owl.Imp().set_as_rule('extends(?x, ?y), extends(?y, ?z) -> extends(?x, ?z)')
        owl.Imp().set_as_rule('extends_DataSet(?x, ?y), has_reference_properties(?y, ?z) -> has_reference_properties(?x, ?z)')
        owl.Imp().set_as_rule('extends_DataSet(?x, ?y), has_reference_methods(?y, ?z) -> has_reference_methods(?x, ?z)')

        class AtomicProjectionSchema(owl.Thing): pass
        class ElectronicRepresenation(owl.Thing): pass

        class MolecularDataSet(DataSet): pass
        class has_molecules(MolecularDataSet >> str): pass
        owl.Imp().set_as_rule('extends_DataSet(?x, ?y), has_molecules(?y, ?z) -> has_molecules(?x, ?z)')
        for molecule_dataset in ('Becke1986', 'Becke1988', 'Lee1988', 'Chu2004', 'Miehlich1989'):
            MolecularDataSet(molecule_dataset)

        class SolidStateDataSet(DataSet): pass
        class has_solids(SolidStateDataSet >> str): pass
        class DensityType(ElectronicRepresenation): pass
        for density in ('ElectronDensity', 'ElectronDensityGradient', 'ElectronDensityLaplacian', 'KineticEnergyDensity'):
            DensityType(density)

        class WavefunctionType(ElectronicRepresenation): pass
        WavefunctionType('SlaterDeterminant')
    return onto
