#!/usr/bin/env python3
import fowl_nomad as fn

abbrev_fowl = fn.Fowl(annot_vis='abbrev')

assert set(abbrev_fowl.expose(root_class='RungOnJacobsLadder', annot_type='nomad_jacobsladder')) == set([
    'LDA', 'GGA', 'Meta-GGA', 'Hybrid', 'Hyper-GGA', 'Double Hybrid'])
assert set(abbrev_fowl.expose(annot_type='abbrev')) == set(['B3LYP', 'B3LYP3', 'B3LYP5', 'B3PW91', 'DFT', 'DFT+U', 'DHA', 'Double Hybrid', 'GGA', 'HSE03', 'HSE06', 'Hybrid', 'Hyper-GGA', 'KS-DFT', 'LDA', 'Meta-GGA', 'PBE', 'PBE_R', 'Virtual State Functionals', 'vdW', 'PW'])
assert set(abbrev_fowl.expose(root_class='hyperGeneralizedGradientApproximation')) == set([
    'DoubleHyperGeneralizedGradientApproximation', 'Hyper-GGA'])

assert set(abbrev_fowl.lookup('GGA_XC_BEEFVDW').classes()) == set(['ExchangeCorrelation', 'GGA', 'LibxcFunctional'])
assert abbrev_fowl.lookup('GGA_XC_BEEFVDW').classes(annot_type='nomad_jacobsladder') == ['GGA']
assert abbrev_fowl.lookup('GGA_XC_BEEFVDW').classes(root_class='RungOnJacobsLadder') == ['GGA']
assert abbrev_fowl.lookup('GGA_XC_BEEFVDW').classes(annot_type='nomad_jacobsladder', root_class='RungOnJacobsLadder') == ['GGA']
assert set(abbrev_fowl.lookup('HYB_GGA_XC_B3PW91').classes()) == set(['B3PW91', 'Hybrid', 'LibxcFunctional'])

assert set(abbrev_fowl.lookup(['GGA_X_PBE', 'GGA_C_PBE']).classes()) == set(['PBE', 'GGA'])
assert set(abbrev_fowl.lookup(['LDA_C_PW', 'LDA_X']).classes()) == set(['LDA', 'PW'])
