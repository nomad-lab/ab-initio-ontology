#!/usr/bin/env python3
import collections as col
import os
import owlready2 as owl


def _annot(func):
    """"""
    def inner(self, *args, **kwargs):
        outputs = func(self, *args, **kwargs)
        if self.annot_vis:
            for out_index, output in enumerate(outputs):
                try:
                    annots = getattr(self.map[output], self.annot_vis)
                    if annots != []:
                        outputs[out_index] = annots[0]
                except AttributeError:
                    pass
                except TypeError:
                    pass
        return outputs
    return inner


class _Fowl_base():
    """Base class for interacting with F.owl ontology"""
    def __init__(self, **kwargs):
        """Load and store the ontology file (default: F.owl)"""
        self.annot_type = None
        self.annot_vis = kwargs.get('annot_vis', None)
        self.root_class = 'Thing'

        script_dir = os.path.dirname(__file__)
        self.filename = kwargs.get('filename', script_dir + '/F.owl')
        self.onto = owl.get_ontology(self.filename).load()

        self.class_map = {x.name: x for x in list(self.onto.classes())}
        self.class_map['Thing'] = owl.Thing  # add Thing root class
        self.indiv_map = {x.name: x for x in list(self.onto.individuals())}
        self.map = {**self.class_map, **self.indiv_map}
        return

    @_annot
    def expose(self, **kwargs) -> list:
        """Show all classes from root_class (included)
        with a specific annotation type in the order defined by the ontology"""
        annot_type = kwargs.get('annot_type', self.annot_type)
        root_class = kwargs.get('root_class', self.root_class)
        annot_classes = {}
        for desc_index, descendant in enumerate(self.map[root_class].descendants()):
            if descendant.name in self.class_map:
                if annot_type:
                    try:
                        annotation = getattr(descendant, annot_type)
                        if annotation != []:
                            annot_classes[annotation.first()] = descendant.name
                    except AttributeError:
                        pass
                else:
                    annot_classes[desc_index] = descendant.name
        annot_classes = col.OrderedDict(sorted(annot_classes.items()))
        return list(annot_classes.values())


class _Fowl_classification(_Fowl_base):
    """"""
    def __init__(self, entry, **kwargs):
        super().__init__(**kwargs)
        self.entry = entry
        return

    @_annot
    def _classes(self) -> list:
        """"""
        class_names = []
        for entry_class in self.entry.is_a:
            try:
                class_names.append(entry_class.name)
            except AttributeError:
                pass
        return class_names

    def classes(self, **kwargs) -> list:
        """"""
        annot_type = kwargs.get('annot_type', self.annot_type)
        root_class = kwargs.get('root_class', self.root_class)
        classes = []
        exposed = self.expose(annot_type=annot_type, root_class=root_class)
        for cl in self._classes():
            if cl in exposed:
                classes.append(cl)
        return classes


class Fowl(_Fowl_base):
    """"""
    def _lookup_entry(self, entry_name):
        """"""
        try:
            return _Fowl_classification(self.map[entry_name], filename=self.filename, annot_vis=self.annot_vis)
        except KeyError:
            raise ValueError('entry is not an owlready2 class/instance')

    def _clean_onto(self, indiv_name):
        """Remove individual with name indiv_name from the ontology
        Used to undo the changes in self._lookup_entries()"""
        indiv_map = {x.name: x for x in list(self.onto.individuals())}
        if indiv_name in indiv_map.keys():
            owl.destroy_entity(indiv_map[indiv_name])
        return

    def _lookup_entries(self, entry_names):
        """"""
        # TODO use kwargs to lookup DataProperties
        self._clean_onto('nomad_entry')
        with self.onto:
            nomad_entry = owl.Thing('nomad_entry')
            for entry_name in entry_names:
                nomad_entry.has_component.append(self.map[entry_name])
            owl.sync_reasoner_pellet(infer_property_values=True, infer_data_property_values=True)
            nomad_entry.is_a.append(self.onto.has_input.exactly(len(nomad_entry.has_input), self.onto.DensityType))
            owl.sync_reasoner_pellet()
        return _Fowl_classification(self.onto.nomad_entry, filename=self.filename, annot_vis=self.annot_vis)

    def lookup(self, entry_names):
        """"""
        if isinstance(entry_names, str):
            entry_names = [entry_names]
        if isinstance(entry_names, list):
            if len(entry_names) == 1:
                return self._lookup_entry(entry_names[0])
            return self._lookup_entries(entry_names)
        raise ValueError('Expected list of strings')
