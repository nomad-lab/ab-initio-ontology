# Crystal Ontology
## Team
- Some members alos seat in OPTIMADE
- Chris: working on CIF files and Python API
- Emanuele Ghedini: maintainer/ontology expert
- Casper Welzel Andersen: domain expert
## Structure
- 2 ontologies:
    - CIF description + converter
    - EMMO with a crystal subsection

## Perspective
Distinguish between the real model enitity & the measurements
- this allows for experiments to be classiffied and compare whether they agree on the actual structure: represent the accuracy of your dataset
- the other approach would be to adopt a _realism_ vision
In our ontology: physics captured by methdos #perspective
An ontology is a tool to define our ingorance #perspective #quote
Mapping of data into a phyiscal concept is decided by the interpreter (algorithm, person, community)  #perspective
- The process of establishing this is the _symbiotic_ process #doublecheck

Put _polycrystalline_ as property below the _material_ #example
- Distinguish between powder (not necessarily _crystal_ + easy to verify) and a polycrystaline
Each laboratory will have his its own _knowledgebase_ #implemenation
- Don't commit to a single model