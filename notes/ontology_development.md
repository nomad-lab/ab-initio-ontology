# Ab Initio Ontology
## Use cases
## TIB
- Continuous development of ontology: publication timeline?
- Move away from isolated solutions: community following?
- Controlled vocabulary: machine-checking?
- Ontoclick plugin for Chrome
- Swagger API

# Design
- Place all XC functionals at class level and connect libxc entries
    - Generate Hubbard and Hybrids as subclasses
- Rename
    - UnoccupiedOccupiedInteraction
- Reparent ScreeningFunctional
- Remove clutter from DFT and Ab Initio defintions: not complete  #implementation
- Adopt relativistics terminology (Approximation and Hamiltonian) into broader categories #implementation
    - Incorporate orders using data_property/class OR has_component/individuals
- Containerize building modules by conceptual world, not ontology type #implementation
- Generate several ontologies (full, only classes) depending on use case #implementation

# Text Mining

# Workshop
- vdW: intermediate categories?