from setuptools import setup


def main():
    setup(name='abinitioknowledgebase',
          version='0.35',
          description='A generator/API for building/consulting the ab initio metod database.',
          author='Nathan Daelman',
          author_email='nathan.daelman@physik.hu-berlin.de',
          license='APACHE 2.0',
          packages=['abinitioknowledgebase'],
          package_data={
            'abinitioknowledgebase': ['F.owl']
          },
          zip_safe=False)

if __name__ == '__main__':
    main()
