from data_collection import FunctionalKernels
from neo4j_nodes import ReferenceNode, KernelNode, FunctionalNode, XCFunctionalNode, SharedParameter
from neomodel import db
import os
import re
import requests
from typing import List, Dict, Any


crossref_format = 'application/vnd.citationstyles.csl+json'

# functions for extracting references
def complete_doi(doi):
    return f"https://doi.org/{doi}"


def get_crossref_info(doi: str) -> Dict[str, Any]:
    """Extract reference information from CrossRef,
    ready for insertion into ReferenceNode"""
    doi_std = complete_doi(doi)
    response = requests.post(doi_std, headers={'Accept': crossref_format})
    
    citation_info = {}
    citation_refs = []
    if response.status_code == 200:
        response_info = response.json()
        if 'author' in response_info.keys():
            if response_info['author']:
                citation_info['author']: List[str] = []
                for author in response_info['author']:
                    author_name = f'{author["given"]} {author["family"]}' if 'given' in author.keys() else author['family']
                    citation_info['author'].append(author_name)
        if 'published' in response_info.keys():
            citation_info['year'] = str(response_info['published']['date-parts'][-1][0])
        if 'link' in response_info.keys():
            citation_info['url']: List[str] = []
            for link in response_info['link']:
                citation_info['url'].append(link['URL'])
        direct_map = {'title': 'title', 'contaoiner-title': 'journal', 'volume': 'volume',
                     'issue': 'issue', 'page': 'pages', 'DOI': 'doi'}
        for crossref_term, neo4j_term in direct_map.items():
            if crossref_term in response_info.keys():
                citation_info[neo4j_term] = response_info[crossref_term]
        
        citation_refs = response_info['reference'] if 'reference' in response_info.keys() else []
    return citation_info, citation_refs


def get_citation_info(doi_reference: Dict[str, str]) -> Dict[str, Any]:
    """Get the second-order citation information from CrossRef."""
    citation_info = {}
    direct_map = {'journal-title': 'journal', 'volume': 'volume', 'issue': 'issue',
                  'first-page': 'pages', 'year': 'year', 'DOI': 'doi'}
    if doi_reference:
        for crossref_term, neo4j_term in direct_map.items():
            if crossref_term in doi_reference.keys():
                citation_info[neo4j_term] = doi_reference[crossref_term]
    return citation_info


def run_xc(id_number: int, path: str):
    """Run the LibXC functional with the given id number.
    Requires the `path` to the xc-info executable."""

    def extract_functional_data(id_number: int, libxc_output_formatted: List[List[str]]) -> Dict[str, Any]:
        """Extract the functional data from the output of xc-info."""

        kind_map = {'XC_EXCHANGE': 'X', 'XC_CORRELATION': 'C', 'XC_EXCHANGE_CORRELATION': 'XC', 'XC_KINETIC': 'K'}
        func_package = {'libxc_id': id_number}

        func_package['version'] = re.match(r'libxc version ([\d\.]+)', libxc_output_formatted[0][0]).group(1)
        func_package['uid'] = re.match(r'name: (\w+)', libxc_output_formatted[1][1]).group(1)
        func_package['jacobs_ladder'] = re.match(r'family: XC_FAMILY_(\w+)', libxc_output_formatted[1][2]).group(1)
        func_package['comment'] = re.match(r'comment: (.+)', libxc_output_formatted[1][4]).group(1)

        func_package['kind'] = kind_map[re.match(r'kind: (\w+)', libxc_output_formatted[1][3]).group(1)]
        func_name = re.sub(f'{func_package["jacobs_ladder"]}_', '', func_package['uid'].upper())
        if '_' in func_name:
            func_name = re.sub(f'{func_package["kind"]}_', '', func_name)
        func_package['name'] = func_name

        return func_package

    def extract_functional_parameters(libxc_output_formatted: List[List[str]]) -> Dict[str, Any]:
        """Extract the functional parameters from the output of xc-info."""
        
        func_parameters = {'value': [], 'name': [], 'description': []}
        parameter_list = libxc_output_formatted[-1][2:]

        if len(libxc_output_formatted[-1]) > 2:
            for param in parameter_list:
                func_parameters['value'].append(float(param[3:16].strip()))
                func_parameters['name'].append(param[16:25])
                func_parameters['description'].append(param[25:])
                if func_parameters['name'][-1][-1] != ' ':
                    description = re.match(r'^([a-zA-Z0-9\[\]_-]+) (.+)$', func_parameters['description'][-1])
                    try:
                        func_parameters['name'][-1] += description.group(1)
                        func_parameters['description'][-1] = description.group(2)
                    except AttributeError:
                        pass
                func_parameters['name'][-1] = re.sub('^_', '', func_parameters['name'][-1].strip())
                func_parameters['description'][-1] = func_parameters['description'][-1].strip()
        no_parameters = sum([len(param) for param in func_parameters.values()])
        func_parameters = func_parameters if no_parameters / 3 == len(parameter_list) and len(parameter_list) > 0 else {}

        return func_parameters

    def extract_references(libxc_output_formatted: List[List[str]]) -> List[str]:
        """Extract the references from the output of xc-info."""
        ref_package = []
        for chunk in libxc_output_formatted:
            if chunk[0].startswith('Ref'):
                for line in chunk[1:]:
                    doi_flag = re.match(r'doi: (.+)$', line)
                    if doi_flag:
                        ref_package.append(doi_flag.group(1))
        return ref_package

    libxc_output = os.popen(f'{path}/xc-info {id_number}').read()
    libxc_output_formatted = []
    new_chunk = True
    for out in libxc_output.split('\n'):         
        if not out:
            new_chunk = True
        else:
            if new_chunk:
                libxc_output_formatted.append([])
                new_chunk = False
            libxc_output_formatted[-1].append(out.strip())
    if len(libxc_output_formatted) <= 2:
        raise ValueError(f'xc-info {id_number} returned no output.')
            
    return extract_functional_data(id_number, libxc_output_formatted), \
           extract_functional_parameters(libxc_output_formatted), \
           extract_references(libxc_output_formatted)


def set_ref_uid(reference_package: Dict[str, Any], unique_uid: bool=False) -> Dict[str, Any]:
    """Set the reference uid (when possible) and return the package."""
    if 'author' in reference_package.keys() and 'year' in reference_package.keys():
        doi = reference_package['doi']
        lastname = reference_package['author'][0].split()[-1]
        uid = f'{lastname}{reference_package["year"]}'
        
        similar_handles = []
        if unique_uid:
            for ref in ReferenceNode.nodes:
                if ref.uid:
                    if ref.uid.startswith(uid) and ref.doi != doi:
                        similar_handles.append(ref.uid)
                    
            if similar_handles:
                last_similar_handle = sorted(similar_handles)[-1]
                if re.match(r'\d', last_similar_handle[-1]):
                    uid += 'a'
                else:
                    re.sub(r'[a-zA-Z]$', chr(ord(uid[-1]) + 1), last_similar_handle[-1])
        reference_package['uid'] = uid
    return reference_package


def write_functional_neighborhood(id_number: int, path: str, secondorder_crossref: bool=False) -> None:
    """Create nodes for the (XC) functionals, their kernels and their references, with their connections."""
    func_package, func_parameters, ref_package = run_xc(id_number, path=path)
    # Set functional, its kernel and their connection
    functional = XCFunctionalNode.create_or_update(func_package)[0] if func_package['kind'] == 'XC' else FunctionalNode.create_or_update(func_package)[0]
    projected_parameters = {term: func_parameters[term] for term in ['value', 'name'] if term in func_parameters.keys()}
    kernel_parameters = {}
    try:
        kernel_parameters['uid'] = FunctionalKernels.invert()[functional.uid]
        for term in ['name', 'description']:
            if term in func_parameters.keys():
                kernel_parameters['parameter_' + term] = func_parameters[term]
        kernel = KernelNode.create_or_update(kernel_parameters)[0]
        functional.parameters.connect(kernel, projected_parameters)
    except KeyError:
        pass
    
    # set references up to 2nd order and interconnect them
    center_nodes = []
    for doi in ref_package:
        citation_info, citation_refs = get_crossref_info(doi)
        if citation_info:
            center_node = ReferenceNode.create_or_update(citation_info)
            center_nodes += center_node
            secondorder_citations = []
            for ref in citation_refs:
                if 'DOI' in ref.keys():
                    secondorder_citations.append(set_ref_uid(get_crossref_info(ref['DOI'])[0]) if secondorder_crossref else get_citation_info(ref))
            reference_nodes = ReferenceNode.create_or_update(*secondorder_citations)
            [center_node[0].references.connect(ref) for ref in reference_nodes]
    if center_nodes:
        [functional.references.connect(center) for center in center_nodes]


def xc_bridge() -> None:
    """Connect X and C functionals to form XC."""
    jacobs_hierachy = {'LDA': 0, 'GGA': 1, 'MGGA': 2}
    Xs, Cs = {}, {}
    X_nodes, C_nodes = {}, {}
    for functional in FunctionalNode.nodes:
        if functional.kind == 'X':
            Xs[functional.name] = functional.jacobs_ladder
            X_nodes[functional.name] = functional
        elif functional.kind == 'C':
            Cs[functional.name] = functional.jacobs_ladder
            C_nodes[functional.name] = functional

    for xc_name in set.intersection(set(Xs.keys()), set(Cs.keys())):
        prepackaged = {'kind': 'XC', 'name': xc_name, 'jacobs_ladder': ''}
        if 'HYB' in Xs[xc_name] + '_' + Cs[xc_name]:
            prepackaged['jacobs_ladder'] += 'HYB_'
        remove_hybrid = lambda x: re.search(r'([A-Z]+)$', x).group(1)
        prepackaged['jacobs_ladder'] += remove_hybrid(max(Xs[xc_name], Cs[xc_name],
                                            key=lambda x: jacobs_hierachy[remove_hybrid(x)]))
        prepackaged['uid'] = f'{prepackaged["jacobs_ladder"]}_xc_{xc_name}'.lower()
    
        XC_node = XCFunctionalNode.create_or_update(prepackaged)[0]
        XC_node.composition.connect(X_nodes[xc_name])
        XC_node.composition.connect(C_nodes[xc_name])


def establish_significant_parameters() -> None:
    """Aggregate common parameters into their own nodes."""

    query = """
        match (m:FunctionalNode)-[l:HAS_PARAMETERS]->(:KernelNode)<-[r:HAS_PARAMETERS]-(n:FunctionalNode)
        return m, n, l.name, l.value, r.name, r.value
    """
    results, _ = db.cypher_query(query, resolve_objects=True)
    for result in results:
        parameters = [result[i][0] for i in range(2, 6) if result[i]]
        if len(parameters) == 4:
            for l_name, l_value, r_name, r_value in zip(*parameters):
                if l_name == r_name and l_value == r_value:
                    try:
                        shared_parameter = SharedParameter.nodes.get(name=l_name, value=l_value)
                    except SharedParameter.DoesNotExist:
                        shared_parameter = SharedParameter.create({'name': l_name, 'value': l_value})[0]
                    result[0].shares.connect(shared_parameter)
                    result[1].shares.connect(shared_parameter)
