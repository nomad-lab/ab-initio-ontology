from neo4j_nodes import FunctionalNode, XCFunctionalNode

def add_manually():
    # 
    uids_starts = ['hyb_mgga_xc_mpw1kcis', 'hyb_mgga_xc_mpwkcis1k', 'hyb_mgga_xc_pbe1kcis', 'hyb_mgga_xc_tpss1kcis']
    uids_ends = ['gga_x_mpw91', 'gga_x_mpw91', 'gga_x_pbe', 'mgga_x_tpss']
    alphas = [.15, .41, .22, .13]

    for uid_start, uid_end, alpha in zip(uids_starts, uids_ends, alphas):
        uid_start = XCFunctionalNode.nodes.get(uid=uid_start)
        uid_end = FunctionalNode.nodes.get(uid=uid_end)
        uid_start.composition.connect(uid_end)

def connect_x_c():
    for functional in FunctionalNode.nodes:
        x, c = None, None
        try:
            if functional.type == 'X':
                x = functional
                c = FunctionalNode.nodes.get(uid=x.uid.replace('_x', '_c'))
            elif functional.type == 'C':
                c = functional
                x = FunctionalNode.nodes.get(uid=c.uid.replace('_c_', '_x_'))
        except FunctionalNode.DoesNotExist:
            pass
        if x and c:
            xc_uid = x.uid.replace('_x_', '_xc_')
            xc = XCFunctionalNode(uid=xc_uid, jacobs_ladder=x.jacobs_ladder, name=x.name, type='XC').save()  # correct composition Jacob's ladder
            xc.composition.connect(x)
            xc.composition.connect(c)
