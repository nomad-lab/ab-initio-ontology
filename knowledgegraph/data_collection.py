import numexpr
from typing import List, Dict, Any, Union
import re
from util import extract_brackets_lines, split_string_list


def regularize_reference_handle(handle: str) -> str:
    return re.sub(r'^&?xc_ref_', '', handle)


def regularize_functional_handle(handle: str) -> str:
    return re.sub(r'^xc_func_info_', '', handle)


class MDataCollector(type):
    """Metaclass for making classes of its type act as a set."""
    def __iter__(cls):
        return iter(cls._all)

    @classmethod
    def _chain(meta, cls, other) -> Dict[Any, Any]:
        if other.key_names[1] != cls.key_names[0]:
            raise ValueError('`key_names` must be chainable')
        _all = {}
        for k, v in other._all.items():
            if isinstance(v, list):
                new_values = []
                for vv in v:
                    try:
                        answer = cls._all[vv]
                    except KeyError:
                        continue
                    if isinstance(answer, list):
                        new_values.extend(answer)
                    else:
                        new_values.append(answer)
                _all[k] = new_values
            elif v in cls._all.keys():
                _all[k] = cls._all[v]
        return _all

    def __mul__(cls, other):
        if type(cls) == type(other) == __class__:
            if not (cls._all and other._all):
                raise ValueError('Both classes must have data stored in them')
            return MDataCollector('to_be_overwritten', (DataCollector, ),
                                {'_all': __class__._chain(cls, other),
                                'key_names': [other.key_names[0], cls.key_names[1]]})
        else:
            raise ValueError('Both classes must be of type `MDataCollector`')
        
    def invert(cls):
        """Invert _all, so all of the values point towards their respective keys."""
        # create an inverted dictionary, with values in lists
        inverted = {}
        for k, v in cls._all.items():
            v = [v] if not isinstance(v, list) else v
            for vv in v:
                if vv in inverted.keys():
                    inverted[vv].append(k)
                else:
                    inverted[vv] = [k]

        # flatten out the trivial lists
        flattened_inversion = []
        for v in inverted.values():
            flattened_inversion.extend(v)
        if len(inverted.values()) == len(flattened_inversion):
            inverted = {k: v[0] for k, v in inverted.items()}
        return inverted


class DataCollector(metaclass=MDataCollector):
    key_names: List[str] = ['', '']

    def __new__(cls, data: Dict[str, Any], **kwargs):
        # check if the names are set
        if len(cls.key_names) != 2:
            raise ValueError('key_names must be a list of length 2')
        cls._all = {} if not hasattr(cls, '_all') else cls._all
        new_data = cls.regularize(data, **kwargs)
        if new_data:
            for k, v in new_data.items():
                cls._all[k] = v
        return super().__new__(cls)
    
    @classmethod
    def regularize(cls, data: Dict[str, Any], **kwargs) -> Union[Dict[str, Any], None]:
        """Regularize the data to be stored."""
        return data


class ReferenceData(DataCollector):
    key_names = ['reference_handle', 'reference_data']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, Any]:
        new_data = {}
        for k, v in data.items():
            k = regularize_reference_handle(k)
            new_data[k] = v
        return super().regularize(new_data, **kwargs)
    
    
class GlobalVariables(DataCollector):
    key_names = ['global_variable_handle', 'global_variable']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, Any]:
        new_data: Dict[str, float] = {}
        for k, v in data.items():
            try:
                new_data[k] = float(v)
            except ValueError:
                new_v = v.split()
                for vk, vv in enumerate(new_v):
                    try:
                        new_v[vk] = new_data[vv]
                    except KeyError:
                        pass
                try:
                    new_data[k] = numexpr.evaluate(new_v)
                except ValueError:
                    pass
        return super().regularize(new_data, **kwargs)


class FunctionalKernels(DataCollector):
    key_names = ['kernel_name', 'functional_handle']
    
    @classmethod
    def regularize(cls, data: Dict[str, List[str]], **kwargs) -> Dict[str, List[str]]:
        new_data = {}
        for k, v in data.items():
            k = k[1:-1].split('/')[-1]
            k = re.sub(r'\.c$', '', k)
            v = [regularize_functional_handle(vv) for vv in v]
            new_data[k] = v
        return super().regularize(new_data, **kwargs)
    
    
class FunctionalIndex(DataCollector):
    key_names = ['functional_name', 'functional_index']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, int]:
        new_data = {}
        for k, v in data.items():
            new_data[k] = int(v)
        return super().regularize(new_data, **kwargs)
    
    
class ParameterNameData(DataCollector):
    key_names = ['parameter_name_handle', 'parameter_name']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Union[Dict[str, List[str]], None]:
        new_data = {}
        filename = kwargs.get('filename')
        for k, v in data.items():
            if 'desc' in k:
                continue
            if filename:
                k = '{}/{}'.format(filename, k)
            v = split_string_list(v)
            v = [re.sub(r'["_]', '', vx) for vx in v if vx]
            v = [vx for vx in v if not re.search(r'\/\*[\w\d\s]+\*\/', vx)]
            new_data[k] = v
        return super().regularize(new_data, **kwargs)


class ParameterValueData(DataCollector):
    key_names = ['parameter_value_handle', 'parameter_value']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, float]:
        new_data = {}
        filename = kwargs.get('filename')
        for k, v in data.items():
            if filename:
                k = '{}/{}'.format(filename, k)
            v = re.sub(r'\/\* [\w\d\s;,-<>\/\.\*\[\]\(\)=]+\*\/', '', v)
            v = re.sub(r'//[\w\d\s;]+,', ',', v)
            v = re.sub(r'L', '', v)
            v = split_string_list(v)
            v = [cls._apply_variables(vv) for vv in v]
            try:
                v = [numexpr.evaluate(vv).item() for vv in v if vv]  # https://stackoverflow.com/questions/43836866/safely-evaluate-simple-string-equation
            except KeyError:
                pass
            new_data[k] = v
        return super().regularize(new_data, **kwargs)
    
    @classmethod
    def _apply_variables(cls, expression: str) -> List[str]:
        for variable, value in GlobalVariables._all.items():
            expression = re.sub(variable, str(value), expression)
            expression = re.sub(r'^\s+', '', expression)
        return expression


class FunctionalNames(DataCollector):
    key_names = ['functional_handle', 'functional_name']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, str]:
        new_data = {}
        for k, v in data.items():
            k = regularize_functional_handle(k)
            new_data[k] = v
        return super().regularize(new_data, **kwargs)


class FunctionalReferences(DataCollector):
    key_names = ['functional_handle', 'reference_handle']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, str]:
        new_data = {}
        for k, v in data.items():
            k = regularize_functional_handle(k)
            v = split_string_list(extract_brackets_lines(v)[0])
            v = [regularize_reference_handle(v) for v in v if v != 'NULL']
            new_data[k] = v
        return super().regularize(new_data, **kwargs)
    
    
class FunctionalParametersName(DataCollector):
    key_names = ['functional_handle', 'parameter_name_handle']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Union[Dict[str, str], None]:
        new_data = {}
        filename = kwargs.get('filename')
        for k, v in data.items():
            k = regularize_functional_handle(k)
            v = split_string_list(extract_brackets_lines(v)[0])[1]
            if filename:
                v = '{}/{}'.format(filename, v)
            new_data[k] = v
        return super().regularize(new_data, **kwargs)

    
class FunctionalParametersValue(DataCollector):
    key_names = ['functional_handle', 'parameter_value_handle']

    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Union[Dict[str, str], None]:
        new_data = {}
        filename = kwargs.get('filename')
        for k, v in data.items():
            k = regularize_functional_handle(k)
            v = split_string_list(extract_brackets_lines(v)[0])[3]
            if filename:
                v = '{}/{}'.format(filename, v)
            new_data[k] = v
        return super().regularize(new_data, **kwargs)


class FunctionalHandleConnections(DataCollector):
    key_names = ['functional_handle', 'connection_handle']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, List[str]]:
        new_data = {}
        for k, v in data.items():
            k = regularize_functional_handle(k)
            v = split_string_list(v)
            v = [vv for vv in v if vv != 'NULL']
            new_data[k] = v
        return super().regularize(new_data, **kwargs)

    
class FunctionalConnectionNames(DataCollector):
    key_names = ['connection_handle', 'functional_name']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, List[str]]:
        new_data = {}
        for k, v in data.items():
            v = split_string_list(extract_brackets_lines(v)[0])
            new_data[k] = v
        return super().regularize(new_data, **kwargs)
    

class FunctionalConnectionCoefficients(DataCollector):
    key_names = ['connection_handle', 'coefficient']
    
    @classmethod
    def regularize(cls, data: Dict[str, str], **kwargs) -> Dict[str, List[float]]:
        new_data = {}
        for k, v in data.items():
            v = split_string_list(extract_brackets_lines(v)[0])
            v = [float(regularize_functional_handle(vx)) for vx in v]
            new_data[k] = v
        return super().regularize(new_data, **kwargs)
