from api_to_neo4j import get_citations
from data_collection import (ReferenceData, ParameterNameData, ParameterValueData,
                             FunctionalKernels, FunctionalNames, FunctionalIndex,
                             FunctionalReferences, FunctionalParametersName, FunctionalParametersValue,
                             FunctionalHandleConnections, FunctionalConnectionNames)
from neo4j_nodes import ReferenceNode, KernelNode, FunctionalNode, XCFunctionalNode
import re
from typing import Dict, Any

class WriteKernelNode:
    libxc_data = (ParameterNameData * FunctionalParametersName * FunctionalKernels)
    neo4j_node = KernelNode

    @classmethod
    def write(cls):
        packaged_data = [{'uid': handle, 'parameters': list(set(parameter_name))} for handle, parameter_name in cls.libxc_data._all.items()]
        cls.neo4j_node.create_or_update(*packaged_data)
        print('Kernels written to database.')


class WriteReferenceNode:
    libxc_data = ReferenceData
    neo4j_node = ReferenceNode

    @classmethod
    def convert_reference_info(cls, reference: str) -> Dict[str, Any]:
        starts = []
        stops = {}
        terms = cls.neo4j_node.terms
        for term in terms:
            flag = ',\s+' + str(term) + ' = '
            hit = re.search(flag, reference)
            if hit:
                starts.append(hit.span()[0])
                stops[term] = hit.span()[1]
        spans = {}
        for stop in sorted(stops.values()):
            for start in sorted(starts):
                if start > stop:
                    spans[stop] = start
                    break
            if stop not in spans.keys():
                spans[stop] = len(reference)
        reference_info = {}
        for term in terms:
            if term in stops.keys():
                reference_info[term] = reference[stops[term]:spans[stops[term]]]
        return reference_info

    @classmethod
    def write(cls):
        packaged_data = []
        for handle, reference in cls.libxc_data._all.items():
            reference_infos = cls.convert_reference_info(reference)
            reference_infos['uid'] = handle
            packaged_data.append(reference_infos)
        cls.neo4j_node.create_or_update(*packaged_data)
        print('References written to database.')
        
        # connect cross-references
        all_dois = {node.doi: node for node in ReferenceNode.nodes}
        for node in ReferenceNode.nodes:
            for citation_doi in get_citations(node.doi):
                if not citation_doi in all_dois.keys():
                    packaged_data = [{'uid': citation_doi, 'doi': citation_doi}]
                    added_ref = cls.neo4j_node.create(*packaged_data)
                    all_dois[citation_doi]: added_ref
                node.references.connect(all_dois[citation_doi])
            print(f'Connected up {node.uid}')
        print('All references interconnected')


class WriteFunctionalNode:
    @classmethod
    def convert_functional_name(cls, functional_name: str) -> Dict[str, str]:
        functional_info = {}
        functional_info['jacobs_ladder'] = re.search(r'^XC_((HYB_)?[A-Z]+)_', functional_name).group(1)
        functional_info['type'] = re.search(r'_([XCK]+)', functional_name).group(1)
        potential_name = re.search(r'_[XCK]+_([\w\d]+)$', functional_name)
        if potential_name:
            functional_info['name'] = potential_name.group(1)
        return functional_info

    @classmethod
    def write(cls):
        packaged_data = []
        xc_packaged_data = []
        for handle, functional_name in FunctionalNames._all.items():
            functional_infos = cls.convert_functional_name(functional_name)
            functional_infos['uid'] = handle
            functional_infos['libxc_index'] = FunctionalIndex._all[FunctionalNames._all[handle]]
            if functional_infos['type'] == 'XC':
                xc_packaged_data.append(functional_infos)
            else:
                packaged_data.append(functional_infos)
        FunctionalNode.create_or_update(*packaged_data)
        XCFunctionalNode.create_or_update(*xc_packaged_data)
        print('Functionals written to database.')

        for functional in FunctionalNode.nodes:
            try:
                reference_handles = FunctionalReferences._all[functional.uid]
            except KeyError:
                continue
            for reference_handle in reference_handles:
                try:
                    functional.references.connect(ReferenceNode.nodes.get(uid=reference_handle))
                except ReferenceNode.DoesNotExist:
                    pass
        print('References connected to functionals.')
        
        for kernel in KernelNode.nodes:
            functional_handles = FunctionalKernels._all[kernel.uid]
            for functional_handle in functional_handles:
                try:
                    pv = (ParameterValueData * FunctionalParametersValue)
                    functional = FunctionalNode.nodes.get(uid=functional_handle)
                    functional.parameters.connect(kernel, {'values': pv._all[functional_handle]})
                except FunctionalNode.DoesNotExist:
                    pass
                except KeyError:
                    functional.parameters.connect(kernel)
        print('Kernels connected to functionals.')

        NamesFunctionalsMap = {v: k for k, v in FunctionalNames._all.items()}  # TODO: create a general inverted map feature
        for functional_handle, functional_connections in (FunctionalConnectionNames * FunctionalHandleConnections)._all.items():
            handle = FunctionalNode.nodes.get(uid=functional_handle)
            for functional_connection in functional_connections:
                try:
                    functional = FunctionalNode.nodes.get(uid=NamesFunctionalsMap[functional_connection])
                except (FunctionalNode.DoesNotExist, KeyError):
                    continue
                handle.composition.connect(functional)
        print('Functionals connected to other functional dependencies.')
