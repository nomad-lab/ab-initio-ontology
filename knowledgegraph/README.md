# DFT Knowledgebase

## Installation

### Configuration

The configuration file `config.yaml` is set in the root directory.
The structure is explained in _Neo4J Database_ and _Libxc_.
`knowledgegraph` comes bundled with a template `config.template.yaml`.

### Neo4J Database

`knowledgegraph` stores the generated graph into [Neo4J]((https://neo4j.com/product/neo4j-graph-database/)).
The setup in `config.yaml`, under `neo4j`, can be toggled to match the installation.
The pre-defined values are the defaults for [Neo4J Desktop](https://neo4j.com/download/?ref=get-started-dropdown-cta).

Once the software is installed, generate the actual database to hold the graph. [^a]
The name of the database is irrelevant, only the domain and port (neo4j default `7687`) matter.
The username and password can be set for each database, though the configuration contains again the defaults (`neo4j` and `password`, respectively).

### Libxc

Most of the graph is constructed from [Libxc](https://gitlab.com/libxc/libxc), be it (1) the program itself, or (2) just the source code.
Using (1) a Libxc installation is heavily suggested though, since (2) source code parsing will likely be fully depricated in the future.

The configuration settings for are under `config.yaml > libxc`.
The path to the source folder `src` should always be set, since not all data is returned by the Libxc interface.
To use case (1), the path to the folder containing the `xc-info` executable should also be defined. [^1]

### CrossRef

Citation data is read from the [CrossRef API](https://www.crossref.org/documentation/retrieve-metadata/rest-api/). [^2]
There is no special setup needed here, just a working internet connection.

### Python Environment

A Python virtual environment with version `3.8.10` comes bundled.
It can activated in a shell session via `source .pyenv/bin/activate`.
Use `deactivate` to close the environment again.

## Usage

### Generating Data

The target neo4j database should be up and running when running `knowledgegraph` and the python environment activated.
See _Neo4J Database_ and _Python Environment_ for more information about the respective setups.
From the command line, simply call python on the folder

```bash
python <path_to_module>/knowledgegraph [OPTIONS]
```

There are several options available.
Consult the full list via the option `--help` or `-h`.

The option `--full-second-order` requires some more detailed explanation.
A CrossRef query returns articles, along with their bibliography.
When this option is deactivated, the second order citations are constructed from these bibliographies.
Since these contain data taken directly from the citing article and little else, they are more sparse than a targeted CrossRef query.
Therefore, `knowledgegraph` allows for the extraction via additional API calls, which will typically yield better quality Reference node properties.
Conversely, this setting runs much slower (in the order of hours to a day), and will skip articles without a DOI.
Use with care.

### Database Structure

The graph database contains several concepts in various types of nodes.
Each _node_ contains data in the form of _properties_.
These include

- `FunctionalNode`, with `XCFunctionalNode` as a subcategory, containg the relevant Libxc data (if relevant), the rung on the Jacob's Ladder, and physics being modeled (e.g. correlation).
- `ReferenceNode` containg the bibliographical data of the respective article.
- `KernelNode`s that list the Libxc functions evaluating the actual DFT functionals. They could be interpreted as representing a family of functionals. It contains the parameters / variables that vary among members of the family.
- `SharedParameter`s that highlight reoccuring parameter values in the same context (i.e. kernel).

These nodes are connected _relationships_, which can again be further specified using _properties_.

- `IS_COMPOSED_OF` connects `XCFunctionalNode` to existing Libxc exchange and correlation `FunctionalNode`s.
- `HAS_REFERENCE` connects `FunctionalNode` or `ReferenceNode` to `ReferenceNode`, building a citation tree.
- `HAS_PARAMETERS` connects `FunctionalNode` to their formula in `KernelNode`, while specifying their parameter settings.
- `SHARES_PARAMETER_SETTING_WITH` bundles `FunctionalNode`s to their common `SharedParameter`.

## Disclaimers and Notes

[^a]: `knowledgegraph` assumes an empty database. To wipe a database, run the cypher query

```cypher
match (n) detach delete n
```

Running the constructer will however complete, not overwrite, a previously generated `knowledgegraph`. This option can be safely used within the same version.
There is no assurance for the data integrety, be it already existing or newly added, in any other case.

[^1]: The user executing `knowledgegraph` should also have the permissions to run or access these folders.

[^2]: At the moment, `knowledgegraph` requires the DOI for look-ups.
