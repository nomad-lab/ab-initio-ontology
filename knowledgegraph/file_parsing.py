from abc import ABC
from typing import List
from interpretation import Interpreter


class FileParser(ABC):
    """Abstract class for file parsers.
    The file parser runs over the code in `chunk`s and calls the `Interpreter`s sequentially."""
    
    def __init__(self, filename: str, interpreters: List[Interpreter], as_chunks: bool = True) -> None:
        if not isinstance(interpreters, list):
            interpreters = [interpreters]
        self._interpreters = interpreters
        if as_chunks:
            self._chunks = self._split_in_chunks(filename)
        else:
            self._chunks = [[line.strip() for line in open(filename, 'r').readlines() if line.strip()]]

    def _split_in_chunks(self, filename: str) -> List[List[str]]:
        """Split the file into chunks."""
        chunks = []
        is_previous_line_blank = True
        in_body = False
        f = open(filename, 'r')
        for line in f.readlines():
            if line.startswith('{'):
                in_body = True
            elif line.startswith('}'):
                in_body = False
            line = line.strip('\n')
            if line or in_body:
                if is_previous_line_blank:
                    chunks.append([line])
                    is_previous_line_blank = False
                else:
                    chunks[-1].append(line)
            else:
                is_previous_line_blank = True
        f.close()
        return chunks
    
    def parse(self, **kwargs) -> None:
        for chunk in self._chunks:
            for interpreter in self._interpreters:
                interpreter(chunk, **kwargs)
