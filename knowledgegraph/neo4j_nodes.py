from neomodel import (StructuredNode, StructuredRel, StringProperty, IntegerProperty, FloatProperty, ArrayProperty, RelationshipTo)


class ReferenceNode(StructuredNode):
    terms = ('author', 'title', 'journal', 'volume', 'number', 'issue', 'pages', 'year', 'doi', 'url')
    uid = StringProperty()
    author = StringProperty()
    title = StringProperty()
    journal = StringProperty()
    volume = StringProperty()
    issue = StringProperty()
    pages = StringProperty()
    year = StringProperty()
    doi = StringProperty(unique_index=True)
    url = StringProperty()
    references = RelationshipTo('ReferenceNode', 'HAS_REFERENCE')


class KernelNode(StructuredNode):
    uid = StringProperty(unique_index=True)
    parameter_name = StringProperty()
    parameter_description = StringProperty()


class KernelParameterRel(StructuredRel):
    name = ArrayProperty()
    value = ArrayProperty()


class FunctionalNode(StructuredNode):
    uid = StringProperty(unique_index=True)
    jacobs_ladder = StringProperty()
    kind = StringProperty()
    name = StringProperty()
    libxc_id = IntegerProperty()
    comment = StringProperty()
    references = RelationshipTo('ReferenceNode', 'HAS_REFERENCE')
    parameters = RelationshipTo('KernelNode', 'HAS_PARAMETERS', model=KernelParameterRel)
    composition = RelationshipTo('FunctionalNode', 'IS_COMPOSED_OF')
    shares = RelationshipTo('SharedParameter', 'SHARES_PARAMETER_SETTING_WITH')


class XCFunctionalNode(FunctionalNode):
    pass


class SharedParameter(StructuredNode):
    name = StringProperty()
    value = FloatProperty()
