import requests

# curl -LH "Accept: application/vnd.citationstyles.csl+json, application/rdf+xml" https://doi.org/10.1126/science.169.3946.635

doi = '10.1126/science.169.3946.635'
app = 'application/vnd.citationstyles.csl+json'
response = requests.post(f'https://doi.org/{doi}', headers={'Accept': app})
print(response.json()['reference'])
