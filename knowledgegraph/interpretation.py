from abc import ABC
from data_collection import (DataCollector, ReferenceData, GlobalVariables, ParameterNameData, ParameterValueData,
                             FunctionalKernels, FunctionalIndex, FunctionalNames,
                             FunctionalReferences, FunctionalParametersName, FunctionalParametersValue,
                             FunctionalHandleConnections, FunctionalConnectionNames, FunctionalConnectionCoefficients)
import re
from typing import List, Dict, Any, Union
from util import extract_brackets_body, extract_brackets_lines


initializer_pattern = re.compile(r'([\w\d]+_init)')


def extract_functional_handle(line: str) -> str:
    """Extract the functional handles from a line."""
    handle_line = line.split()
    if len(handle_line) == 5:
        if handle_line[:2] + handle_line[3:] == ['const', 'xc_func_info_type', '=', '{']:
            return handle_line[2]


class Interpreter(ABC):
    """Abstract class for interpreters.
    It extracts the relevant data from a list of strings and writes it to `data_collector_type`."""
    def __new__(cls, chunk: List[str], **kwargs):
        if not hasattr(cls, 'data_collector_type'):
            raise ValueError('`data_collector_type` must be set')
        data = cls.interprete(chunk)
        if data:
            cls.data_collector_type(data, **kwargs)
        return super().__new__(cls)

    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, Any], None]:
        """The function to be called for interpretation."""
        pass


class GetReference(Interpreter):
    data_collector_type = ReferenceData
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, str], None]:
        first_line = chunk[0].split()
        if first_line[0] == 'func_reference_type':
            handle = first_line[1]
            parsed_chunk = []
            for line in chunk[1:-1]:
                for symbol in ['\\n', '\"', '{', '}', '\\']:
                    line = line.replace(symbol, '')
                parsed_chunk.append(line)
            return {handle: ' '.join(parsed_chunk)}
        return {}


class GetGlobalVariable(Interpreter):
    data_collector_type = GlobalVariables
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, str], None]:
        parsed_chunk = {}
        for line in chunk:
            definition = re.match(r'#\s*define\s+([\w\d]+)\s+([\d\.]+)', line)
            if definition:
                parsed_chunk[definition.group(1)] = definition.group(2)
        return parsed_chunk


class GetParameterData(Interpreter):
    data_collector_type = DataCollector
        
    @classmethod
    def interprete(cls, chunk: List[str], regex='') -> Union[Dict[str, str], None]:
        data = {}
        chunk_not_finsihed = True
        while chunk_not_finsihed:
            for lnum, line in enumerate(chunk):
                flag = re.match(regex, line)
                if flag:
                    handle = flag.group(1)
                    if '[][' in line:
                        return None
                    extraction, chunk = extract_brackets_lines(chunk[lnum:])
                    data[handle] = extraction
                    if not chunk:
                        chunk_not_finsihed = False
                    break
                elif lnum == len(chunk) - 1:
                    chunk_not_finsihed = False
        if data:
            return data


class GetParameterNameData(GetParameterData):
    data_collector_type = ParameterNameData
    
    @classmethod
    def interprete(cls, chunk: List[str],
                   regex=r'static const char\s+\*(\w+)\[') -> Union[Dict[str, str], None]:
        return super().interprete(chunk, regex=regex)
    

class GetParameterValueData(GetParameterData):
    data_collector_type = ParameterValueData
    
    @classmethod
    def interprete(cls, chunk: List[str],
                   regex=r'static const double\s+(\w+)\[') -> Union[Dict[str, str], None]:
        return super().interprete(chunk, regex=regex)


class GetFunctionalKernels(Interpreter):
    """Interpreter for getting the functional kernels."""
    data_collector_type = FunctionalKernels

    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, str], None]:
        """Extract the functional kernels from the chunk."""
        functional_handles = []
        kernel_name: str = None
        for line in chunk:
            if '#include' in line and 'maple2c' in line:
                kernel_name = line.split()[1]
            else:
                handle = extract_functional_handle(line)
                if handle:
                    functional_handles.append(handle)
        if functional_handles and kernel_name:
            return {kernel_name: functional_handles}


class GetFunctionalIndex(Interpreter):
    """Interpreter for getting the functional index."""
    data_collector_type = FunctionalIndex

    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, str], None]:
        """Extract the functional index from the code block."""
        parsed = {}
        for line in chunk:
            line = line.split()
            if len(line) < 3:
                continue
            if line[0] == '#define' and 'XC_' in line[1]:
                parsed[line[1]] = line[2]
        if parsed:
            return parsed
        return None


class GetFunctionalBody(Interpreter):  # TODO: make abstract
    """Interpreter for getting the functional parameters."""
    data_collector_type = DataCollector
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, str], None]:
        if len(chunk) > 5:
            flags: List[bool] = [chunk[i] == x for i, x in enumerate(['#ifdef __cplusplus', 'extern "C"', '#endif'])]
            if all(flags):
                function_handle = extract_functional_handle(chunk[3])
                body = extract_brackets_body(chunk)[0]
                if function_handle and body:
                    return {function_handle: body}


class GetFunctionalNames(GetFunctionalBody):
    data_collector_type = FunctionalNames
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, str], None]:
        interpretation = super().interprete(chunk)
        if interpretation:
            function_handle, body = list(interpretation.items())[0]
            for line in body:
                if re.match('XC_', line):
                    return {function_handle: line}


class GetFunctionalReferences(GetFunctionalBody):
    data_collector_type = FunctionalReferences
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, List[str]], None]:
        interpretation = super().interprete(chunk)
        if interpretation:
            function_handle, body = list(interpretation.items())[0]
            for line in body:
                if re.match('{&xc_ref', line):
                    return {function_handle: line}


class GetFunctionalHandleConnections(GetFunctionalBody):   
    data_collector_type = FunctionalHandleConnections
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, List[str]], None]:
        interpretation = super().interprete(chunk)
        if interpretation:
            function_handle, body = list(interpretation.items())[0]
            for line in body:
                if initializer_pattern.match(line):
                    return {function_handle: line}

                  
class GetFunctionalParameters(GetFunctionalBody):
    data_collector_type = DataCollector
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, List[str]], None]:
        interpretation = super().interprete(chunk)
        if interpretation:
            function_handle, body = list(interpretation.items())[0]
            for line in body:
                if re.match('{N_PAR', line):
                    return {function_handle: line}


class GetFunctionalParametersNames(GetFunctionalParameters):
    data_collector_type = FunctionalParametersName


class GetFunctionalParametersValues(GetFunctionalParameters):
    data_collector_type = FunctionalParametersValue


class GetFunctionalConnection(Interpreter):
    data_collector_type = DataCollector
    
    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, List[str]], None]:
        handle = None
        if 'xc_mix_init' not in ' '.join(chunk):
            return None
        for lnum, line in enumerate(chunk):
            initializer_handle = initializer_pattern.match(line)
            if initializer_handle:
                handle = initializer_handle.group(1)
            if line.startswith('{'):
                return {handle: extract_brackets_body(chunk[lnum:])[0]}
            

class GetFunctionalConnectionNames(GetFunctionalConnection):
    data_collector_type = FunctionalConnectionNames

    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, List[str]], None]:
        interpretation = super().interprete(chunk)
        if interpretation:
            handle, body = list(interpretation.items())[0]
            for line in body:
                if re.search(r'int\s+funcs_id', line):
                    return {handle: line}


class GetFunctionalConnectionCoefficients(GetFunctionalConnection):
    data_collector_type = FunctionalConnectionCoefficients

    @classmethod
    def interprete(cls, chunk: List[str]) -> Union[Dict[str, List[str]], None]:
        interpretation = super().interprete(chunk)
        if interpretation:
            handle, body = list(interpretation.items())[0]
            for line in body:
                if re.search(r'double\s+funcs_coeff', line):
                    return {handle: line}
