import re
from typing import Union, List, Tuple


def extract_brackets_body(lines: Union[str, List[str]],
                          brackets: List[str]=['{', '}']) -> Tuple[Union[List[str], None], List[str]]:
    def shift_line(line: str) -> str:
        indendation = re.compile(r'^\s+')
        ending = re.compile(r',$')
        line = indendation.sub('', line)
        line = ending.sub('', line)
        return line    

    if len(brackets) != 2:
        raise ValueError('The brackets list must contain exactly two elements.')
    new_lines = []
    has_started = False
    for lnum, line in enumerate(lines):
        if not has_started and brackets[0] in line:
            has_started = True
        elif has_started:
            if re.match(brackets[1], line):
                break
            else:
                new_lines.append(shift_line(line))
    return new_lines, lines[lnum+1:]
    

def extract_brackets_lines(lines: Union[str, List[str]],
                           brackets: List[str]=['{', '}']) -> Tuple[Union[List[str], None], List[str]]:
    def trim(line: str) -> str:
        return re.sub(r',\s*$', '', line)
    
    if len(brackets) != 2:
        raise ValueError('The brackets list must contain exactly two elements.')
    if isinstance(lines, str):
        lines = [lines]
    new_lines = []
    has_started = False
    for lnum, line in enumerate(lines):
        if not has_started and brackets[0] in line:
            has_started = True
            line = line.split(brackets[0])[1]
        if has_started:
            if brackets[1] in line:
                line = line.split(brackets[1])[0]
                new_lines.append(trim(line))
                break
            else:
                new_lines.append(trim(line))
    new_lines = ', '.join(new_lines)
    return new_lines, lines[lnum+1:]


def split_string_list(string: str) -> List[str]:
    """Split the string to list of words."""
    return re.split(r',\s*', string)
