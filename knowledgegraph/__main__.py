from api_to_neo4j import write_functional_neighborhood, xc_bridge, establish_significant_parameters
from argparse import ArgumentParser
from file_parsing import FileParser
from interpretation import GetFunctionalKernels
from neomodel import config, db
from os import listdir
from os.path import isfile, join, dirname, realpath
from yaml import safe_load

if __name__ == '__main__':
    # Read command line options
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='./config.yaml', help='point to the setup file, default: `config.yaml`')
    parser.add_argument('--full-second-order', action='store_true', help='extract second order cross references from crossref.org ' +
                        '(WARNING: this will take a long time), default: False')
    parser.add_argument('-i', '--libxc_range', type=int, nargs=2, default=[1, 750], help='range in which to parse the Libxc functionals, default: 1 750')
    parser.add_argument('-v', '--verbose', action='store_true', help='print progress, default: False')
    args = parser.parse_args()

    # read configuration file variables
    config_file = f'{dirname(realpath(__file__))}/{args.config}' if args.config.startswith('.') else args.config
    with open(config_file, 'r') as f:
        config_vars = safe_load(f)
    config.DATABASE_URL = f'bolt://{config_vars["neo4j"]["user"]}:{config_vars["neo4j"]["password"]}@{config_vars["neo4j"]["host"]}:{config_vars["neo4j"]["port"]}'

    # parse the LibXC files
    '''to_be_parsed = [ReferencesParser(f'{config_vars["libxc"]["src"]}/references.c'),
                    GlobalVariablesParser(f'{config_vars["libxc"]["src"]}/util.h')]
    for parsing in to_be_parsed:
        parsing.parse()'''
        
    source_content = [f for f in listdir(config_vars["libxc"]["src"]) if
                      isfile(join(config_vars["libxc"]["src"], f)) and f.endswith('.c') and f[:3] in ('lda', 'gga', 'mgg', 'hyb')]
    for f in source_content:
        kernel_mapper = FileParser(config_vars["libxc"]["src"] + f, GetFunctionalKernels, as_chunks=False)
        kernel_mapper.parse()
        if args.verbose:
            print(f'Parsed file: {f}')

    # write the data to the neo4J database
    with db.transaction:
        args.libxc_range[1] += 1
        for id in range(*args.libxc_range):
            print(f'Accessing information : Libxc ID {id:4d}') if args.verbose else None
            try:
                write_functional_neighborhood(id, config_vars["libxc"]["xc-info"], secondorder_crossref=args.full_second_order)
                print(f'Found and extracted   : Libxc ID {id:4d}') if args.verbose else None
            except ValueError:
                pass
        xc_bridge()
        establish_significant_parameters()
    